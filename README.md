# Cloudwiry and AWS Lambda in Graviton2

This is a **Serveless Framework** plugin that allow to use AWS Lambda functions powered by Graviton2.

Sponsored by: [Cloudwiry](https://www.cloudwiry.com)

## Usage:

1. Install the plugin

```bash
npm i git@bitbucket.org:teamgenie/serverless-aws-lambda-arch.git
```

2. Add the plugin to your project's `serverless.yml` and mark your functions using the `architectures` 
   property like this sample.

```yml
service: arch-test

plugins:
  - serverless-aws-lambda-arch

provider:
  name: aws
  runtime: python3.9

package:
  patterns:
    - '!node_modules/**'

functions:
  dummyFn1:
    handler: handler
    description: "Test fn1"

  dummyFn2_RunningInARM64:
    handler: handler
    description: "Test fn2"
    architectures: arm64
```

3. Have fun!

```bash
sls deploy
```

## Quick test

```bash
cd test && npm i && sls deploy
```
