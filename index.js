'use strict';

class ServerlessAwsLambdaArchPlugin {
  constructor(serverless, options) {
    this.serverless = serverless;
    this.options = options;

    this.awsProvider = serverless.getProvider('aws');
    this.providerNaming = this.awsProvider.naming;

    serverless.configSchemaHandler.defineFunctionProperties('aws', {
      properties: {
        architectures: {
          type: 'array',
        },
      },
    });

    this.hooks = {
        'package:compileEvents': this.compile.bind(this),
    };
  }

  compile() {
    this.serverless.service.getAllFunctions().forEach((functionName) => {
      const functionObj = this.serverless.service.getFunction(functionName);
      const functionRef = this.providerNaming.getLambdaLogicalId(functionName);
      if(functionObj.architectures) {
        var cf = this.serverless.service.provider.compiledCloudFormationTemplate.Resources[functionRef].Properties;
        cf.Architectures = functionObj.architectures;
      }
    });
  }
}

module.exports = ServerlessAwsLambdaArchPlugin;
